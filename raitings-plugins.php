<?php

/*
Plugin Name: Raitings List
Plugin URI: https://www.wpblog.com/
Description: Raitings
Version: 1.0
*/





/**
 * Adds Foo_Widget widget.
 */
class RaitingsWidget extends WP_Widget {

    /**
     * Register widget with WordPress.
     */
    function __construct() {
        parent::__construct(
            'about_widget', // Base ID
            esc_html__( 'Ratings', 'text_domain' ), // Name
            array( 'description' => esc_html__( 'Widget about company', 'text_domain' ), ) // Args
        );
    }

    /**
     * Front-end display of widget.
     *
     * @see WP_Widget::widget()
     *
     * @param array $args     Widget arguments.
     * @param array $instance Saved values from database.
     */
    public function widget( $args, $instance ) {
        $style = "<style>                      " .
            ".raitingList li{                               " .
            "	position:relative;                         " .
            "	list-style-type: none;                     " .
            "	}                                          " .
            "                                               " .
            ".raitingList{                                  " .
            " 	padding: 0;                                " .
            "}                                              " .
            ".opshi{                                        " .
            "  position: relative;                          " .
            "}                                              " .
            "                                               " .
            ".opshi:hover .haytnvel{                        " .
            "  opacity: 1;                                  " .
            "  background: rgba(62,99,189,0.3)              " .
            "}                                              " .
            "                                               " .
            ".haytnvel{                                     " .
            "   position: absolute;                         " .
            "  top: 0;                                      " .
            "  bottom: 0;                                   " .
            "  left: 0;                                     " .
            "  right: 0;                                    " .
            "  height: 100%;                                " .
            "  width: 100%;                                 " .
            "  opacity: 0;                                  " .
            "  transition: .2s ease;                        " .
            "  display: flex;                               " .
            "  justify-content: center;                     " .
            "}                                              " .
            ".hoverButton{                             " .
            "  height: 40px;                                " .
            "  margin-left: 7px;                            " .
            "  margin-top: 13px;                            " .
            "  font-weight: bold;                           " .
            "                                               " .
            "}                                              " .
            "                                               " .
            ".obzor:hover{                                  " .
            "color:red                                      " .
            "}                                              " .
            "</style>";
        /////////////////
        echo $args['before_widget'];
        if ( ! empty( $instance['title'] ) ) {
            echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
        }
        $list = '<aside id="categories-6" class="">' .
            '<h3 class="widget-title">Рейтинг букмекеров</h3>   <ul class=\'raitingList\'>';

        for($i=1; $i<=14; $i++){

            $list .= '<li class="cat-item cat-item-13 opshi"">'
                . " <div style=\"display: flex;justify-content: center;align-items: baseline;\"> "
                . "	<h4 style=\"width: 20%;color:#ffc000\">$i </h4>"
                . "	<div style=\"width: 48%;\">"
                . "	  <img src=\"" . apply_filters("widget_bukmeker$i", $instance["bukmeker$i"]) . "\" style=\"width: 70%\">"
                . "	</div>"
                . "	<h6 style=\"width: 20%;color:yellow\"> ★★★★★ </h6>"
                . " </div> "
                . "	 <div class=\"haytnvel\" >"

                . "	<button class=\"obzor hoverButton\" style=\"background:#443A54; border:none; color:white; font-style:italic; letter-spacing:2px; font-size:12px\">Обзор</button>"
                ."<a href=\"#\">"
                . "    <button class=\"ej hoverButton\" style=\"background: white;border:none;font-style: italic;color:black;letter-spacing: 1px; font-size:12px\">На Сайт</button>"
                ."</a>"
                . " </div></li>";

        }

        $list .= '</ul></aside>';
        echo $style;
        echo $list;
        echo $args['after_widget'];
    }

    /**
     * Back-end widget form.
     *
     * @see WP_Widget::form()
     *
     * @param array $instance Previously saved values from database.
     */
    public function form( $instance ) {
        $bukmeker1 = ! empty( $instance['bukmeker1'] ) ? $instance['bukmeker1'] : esc_html__( 'Bukmeker1 Name', 'text_domain' );
        $bukmeker2 = ! empty( $instance['bukmeker2'] ) ? $instance['bukmeker2'] : esc_html__( 'Bukmeker2 Name', 'text_domain' );
        $bukmeker3 = ! empty( $instance['bukmeker3'] ) ? $instance['bukmeker3'] : esc_html__( 'Bukmeker3 Name', 'text_domain' );
        $bukmeker4 = ! empty( $instance['bukmeker4'] ) ? $instance['bukmeker4'] : esc_html__( 'Bukmeker4 Name', 'text_domain' );
        $bukmeker5 = ! empty( $instance['bukmeker5'] ) ? $instance['bukmeker5'] : esc_html__( 'Bukmeker5 Name', 'text_domain' );
        $bukmeker6 = ! empty( $instance['bukmeker6'] ) ? $instance['bukmeker6'] : esc_html__( 'Bukmeker6 Name', 'text_domain' );
        $bukmeker7 = ! empty( $instance['bukmeker7'] ) ? $instance['bukmeker7'] : esc_html__( 'Bukmeker7 Name', 'text_domain' );
        $bukmeker8 = ! empty( $instance['bukmeker8'] ) ? $instance['bukmeker8'] : esc_html__( 'Bukmeker8 Name', 'text_domain' );
        $bukmeker9 = ! empty( $instance['bukmeker9'] ) ? $instance['bukmeker9'] : esc_html__( 'Bukmeker9 Name', 'text_domain' );
        $bukmeker10 = ! empty( $instance['bukmeker10'] ) ? $instance['bukmeker10'] : esc_html__( 'Bukmeker10 Name', 'text_domain' );
        $bukmeker11 = ! empty( $instance['bukmeker11'] ) ? $instance['bukmeker11'] : esc_html__( 'Bukmeker11 Name', 'text_domain' );
        $bukmeker12 = ! empty( $instance['bukmeker12'] ) ? $instance['bukmeker12'] : esc_html__( 'Bukmeker12 Name', 'text_domain' );
        $bukmeker13 = ! empty( $instance['bukmeker13'] ) ? $instance['bukmeker13'] : esc_html__( 'Bukmeker13 Name', 'text_domain' );
        $bukmeker14 = ! empty( $instance['bukmeker14'] ) ? $instance['bukmeker14'] : esc_html__( 'Bukmeker14 Name', 'text_domain' );

        ?>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'bukmeker1' ) ); ?>"><?php esc_attr_e( 'Bukmeker1:', 'text_domain' ); ?></label>
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'bukmeker1' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'bukmeker1' ) ); ?>" type="text" value="<?php echo esc_attr( $bukmeker1 ); ?>">
        </p>

        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'bukmeker2' ) ); ?>"><?php esc_attr_e( 'Bukmeker2:', 'text_domain' ); ?></label>
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'bukmeker2' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'bukmeker2' ) ); ?>" type="text" value="<?php echo esc_attr( $bukmeker2 ); ?>">
        </p>

        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'bukmeker3' ) ); ?>"><?php esc_attr_e( 'Bukmeker3:', 'text_domain' ); ?></label>
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'bukmeker3' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'bukmeker3' ) ); ?>" type="text" value="<?php echo esc_attr( $bukmeker3 ); ?>">
        </p>

        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'bukmeker4' ) ); ?>"><?php esc_attr_e( 'Bukmeker4:', 'text_domain' ); ?></label>
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'bukmeker4' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'bukmeker4' ) ); ?>" type="text" value="<?php echo esc_attr( $bukmeker4); ?>">
        </p>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'bukmeker5' ) ); ?>"><?php esc_attr_e( 'Bukmeker5:', 'text_domain' ); ?></label>
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'bukmeker5' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'bukmeker5' ) ); ?>" type="text" value="<?php echo esc_attr( $bukmeker5); ?>">
        </p>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'bukmeker6' ) ); ?>"><?php esc_attr_e( 'Bukmeker6:', 'text_domain' ); ?></label>
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'bukmeker6' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'bukmeker6' ) ); ?>" type="text" value="<?php echo esc_attr( $bukmeker6); ?>">
        </p>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'bukmeker7' ) ); ?>"><?php esc_attr_e( 'Bukmeker7:', 'text_domain' ); ?></label>
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'bukmeker7' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'bukmeker7' ) ); ?>" type="text" value="<?php echo esc_attr( $bukmeker7); ?>">
        </p>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'bukmeker8' ) ); ?>"><?php esc_attr_e( 'Bukmeker8:', 'text_domain' ); ?></label>
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'bukmeker8' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'bukmeker8' ) ); ?>" type="text" value="<?php echo esc_attr( $bukmeker8); ?>">
        </p>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'bukmeker9' ) ); ?>"><?php esc_attr_e( 'Bukmeker9:', 'text_domain' ); ?></label>
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'bukmeker9' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'bukmeker9' ) ); ?>" type="text" value="<?php echo esc_attr( $bukmeker9); ?>">
        </p>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'bukmeker10' ) ); ?>"><?php esc_attr_e( 'Bukmeker10:', 'text_domain' ); ?></label>
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'bukmeker10' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'bukmeker10' ) ); ?>" type="text" value="<?php echo esc_attr( $bukmeker10); ?>">
        </p>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'bukmeker11' ) ); ?>"><?php esc_attr_e( 'Bukmeker11:', 'text_domain' ); ?></label>
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'bukmeker11' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'bukmeker11' ) ); ?>" type="text" value="<?php echo esc_attr( $bukmeker11); ?>">
        </p>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'bukmeker12' ) ); ?>"><?php esc_attr_e( 'Bukmeker12:', 'text_domain' ); ?></label>
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'bukmeker12' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'bukmeker12' ) ); ?>" type="text" value="<?php echo esc_attr( $bukmeker12); ?>">
        </p>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'bukmeker13' ) ); ?>"><?php esc_attr_e( 'Bukmeker13:', 'text_domain' ); ?></label>
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'bukmeker13' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'bukmeker13' ) ); ?>" type="text" value="<?php echo esc_attr( $bukmeker13); ?>">
        </p>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'bukmeker14' ) ); ?>"><?php esc_attr_e( 'Bukmeker14:', 'text_domain' ); ?></label>
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'bukmeker14' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'bukmeker14' ) ); ?>" type="text" value="<?php echo esc_attr( $bukmeker14); ?>">
        </p>

        <?php
    }

    /**
     * Sanitize widget form values as they are saved.
     *
     * @see WP_Widget::update()
     *
     * @param array $new_instance Values just sent to be saved.
     * @param array $old_instance Previously saved values from database.
     *
     * @return array Updated safe values to be saved.
     */
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['bukmeker1'] = ( ! empty( $new_instance['bukmeker1'] ) ) ? sanitize_text_field( $new_instance['bukmeker1'] ) : '';
        $instance['bukmeker2'] = ( ! empty( $new_instance['bukmeker2'] ) ) ? sanitize_text_field( $new_instance['bukmeker2'] ) : '';
        $instance['bukmeker3'] = ( ! empty( $new_instance['bukmeker3'] ) ) ? sanitize_text_field( $new_instance['bukmeker3'] ) : '';
        $instance['bukmeker4'] = ( ! empty( $new_instance['bukmeker4'] ) ) ? sanitize_text_field( $new_instance['bukmeker4'] ) : '';
        $instance['bukmeker5'] = ( ! empty( $new_instance['bukmeker5'] ) ) ? sanitize_text_field( $new_instance['bukmeker5'] ) : '';
        $instance['bukmeker6'] = ( ! empty( $new_instance['bukmeker6'] ) ) ? sanitize_text_field( $new_instance['bukmeker6'] ) : '';
        $instance['bukmeker7'] = ( ! empty( $new_instance['bukmeker7'] ) ) ? sanitize_text_field( $new_instance['bukmeker7'] ) : '';
        $instance['bukmeker8'] = ( ! empty( $new_instance['bukmeker8'] ) ) ? sanitize_text_field( $new_instance['bukmeker8'] ) : '';
        $instance['bukmeker9'] = ( ! empty( $new_instance['bukmeker9'] ) ) ? sanitize_text_field( $new_instance['bukmeker9'] ) : '';
        $instance['bukmeker10'] = ( ! empty( $new_instance['bukmeker10'] ) ) ? sanitize_text_field( $new_instance['bukmeker10'] ) : '';
        $instance['bukmeker11'] = ( ! empty( $new_instance['bukmeker11'] ) ) ? sanitize_text_field( $new_instance['bukmeker11'] ) : '';
        $instance['bukmeker12'] = ( ! empty( $new_instance['bukmeker12'] ) ) ? sanitize_text_field( $new_instance['bukmeker12'] ) : '';
        $instance['bukmeker13'] = ( ! empty( $new_instance['bukmeker13'] ) ) ? sanitize_text_field( $new_instance['bukmeker13'] ) : '';
        $instance['bukmeker14'] = ( ! empty( $new_instance['bukmeker14'] ) ) ? sanitize_text_field( $new_instance['bukmeker14'] ) : '';

        return $instance;
    }

} // class Foo_Widget
// register widget
add_action('widgets_init', create_function('', 'return register_widget("RaitingsWidget");'));